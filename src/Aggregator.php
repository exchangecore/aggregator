<?php
namespace exchangecore\aggregator;

class Aggregator
{

    protected $data = [];
    protected $groupByColumns = [];
    protected $aggregateColumns = [];

    protected $detailsKey = 'details';
    protected $groupingKeyValue = 'groupingValue';
    protected $purgeDetails = false;
    protected $sortFlags = null;

    public function __construct($array)
    {
        $this->data = $array;
    }

    public function groupBy($groupColumns)
    {
        $this->groupByColumns = [];
        return $this->addGroupBy($groupColumns);
    }

    public function addGroupBy($groupColumns)
    {
        if (is_string($groupColumns)) {
            $groupColumns = [$groupColumns];
        }

        if (!is_array($groupColumns)) {
            throw new \Exception('You must use a string or array to group by.');
        }

        $this->groupByColumns = array_merge_recursive($this->groupByColumns, $groupColumns);
        return $this;
    }

    public function aggregationColumns($aggregateColumns)
    {
        $this->aggregateColumns = [];
        return $this->addAggregationColumns($aggregateColumns);
    }

    public function addAggregationColumns($aggregateColumns)
    {
        if (!is_array($aggregateColumns)) {
            throw new \Exception('You must use an array to specify columns to aggregate.');
        }

        $this->aggregateColumns = array_merge_recursive($this->aggregateColumns, $aggregateColumns);
        return $this;
    }


    /**
     * @param null $objectStore When set to a class name, the aggregation will save each aggregated record into an
     *                          instance of the class specified (using public properties)
     * @return $this
     */
    public function aggregate($objectStore = null)
    {
        $aggregatedData = [];
        $reversedGroupBy = array_reverse($this->groupByColumns);
        foreach ($this->data AS &$record) {
            $this->groupRecords($aggregatedData, $reversedGroupBy, $record);
        }
        $this->recursiveKeySort($aggregatedData);
        $this->data = $this->flattenGroupedData($aggregatedData, $reversedGroupBy);
        $this->aggregateData($objectStore);
        if ($objectStore !== null) {
            $this->dataToObjects($objectStore);
        }
        return $this;
    }

    protected function groupRecords(&$groupData, $reversedGroupBy, $record)
    {
        if (empty($reversedGroupBy)) {
            $groupData[$this->detailsKey][] = $record;
            return $groupData;
        }

        $currentGroup = array_pop($reversedGroupBy);

        if (is_callable($currentGroup)) {
            $grouping = $currentGroup($record);
        } elseif (is_object($record)) {
            $grouping = $record->$currentGroup;
        } else {
            $grouping = $record[$currentGroup];
        }

        if (is_object($grouping)) {
            $groupingHash = sha1(serialize($grouping));
        } else {
            $groupingHash = $grouping;
        }

        if (!isset($groupData[$groupingHash])) {
            $groupData[$groupingHash] = [];
        }

        $groupData[$groupingHash][$this->groupingKeyValue] = $grouping;
        $groupData[$groupingHash] = $this->groupRecords($groupData[$groupingHash], $reversedGroupBy, $record);
        return $groupData;
    }

    protected function flattenGroupedData($groupedData, $reversedGroupBy, $mergeWith = [], &$flattenedData = [])
    {
        if (empty($reversedGroupBy)) {
            unset($groupedData[$this->groupingKeyValue]);
            $flattenedData[] = array_merge($mergeWith, $groupedData);
            return false;
        }
        $return = [];

        if (is_callable(end($reversedGroupBy))) {
            $groupKeyValue = key($reversedGroupBy);
        } else {
            $groupKeyValue = current($reversedGroupBy);
        }
        array_pop($reversedGroupBy);

        foreach ($groupedData AS $groupKey => $groupedRecord) {
            if (is_array($groupedRecord) && $groupKey !== $this->groupingKeyValue) {
                $return[] = $this->flattenGroupedData(
                    $groupedRecord,
                    $reversedGroupBy,
                    array_merge($mergeWith, [$groupKeyValue => $groupedRecord[$this->groupingKeyValue]]),
                    $flattenedData
                );
            } else {
                $return[$groupKey] = $groupedRecord;
            }
        }

        return $flattenedData;
    }

    public function getData()
    {
        return $this->data;
    }

    protected function aggregateData()
    {
        foreach ($this->data AS &$aggregatedData) {
            foreach ($this->aggregateColumns AS $key => $aggregation) {
                if (is_callable($aggregation)) {
                    $callback = $aggregation;
                    $initial = null;
                } elseif (isset($aggregation['callback']) && isset($aggregation['initial'])) {
                    $callback = $aggregation['callback'];
                    $initial = $aggregation['initial'];
                } else {
                    throw new \Exception('The aggregation must be a closure function or have the callback and initial array keys set');
                }
                $aggregatedData[$key] = array_reduce(
                    $aggregatedData[$this->detailsKey],
                    $callback,
                    $initial
                );
            }
            if($this->purgeDetails) {
                unset($aggregatedData[$this->detailsKey]);
            }
        }
    }

    /**
     * When performing an aggregation operation, the underlying details will be stored beneath the array key specified
     * by this function.
     *
     * @param string $key
     * @return $this
     */
    public function setDetailsKey($key = 'details')
    {
        $this->detailsKey = $key;
        return $this;
    }

    /**
     * When performing a group by operation, this is the key in the array that is temporarily used for storing the value
     * of the group by field. This is needed to accommodate grouping by objects (which are serialized and hashed for
     * comparison)
     *
     * @param string $key
     * @return $this
     */
    public function setGroupingKeyValue($key = 'groupingValue')
    {
        $this->groupingKeyValue = $key;
        return $this;
    }

    /**
     * @param null|int $flags See sort() function for usable flags
     * @return $this
     */
    public function setSortFlags($flags = null)
    {
        $this->sortFlags = $flags;
        return $this;
    }

    public function purgeDetails($value = true)
    {
        $this->purgeDetails = $value;
        return $this;
    }

    protected function recursiveKeySort(&$array)
    {
        foreach ($array AS &$value) {
            if (is_array($value)) {
                $this->recursiveKeySort($value);
            }
        }
        return ksort($array, $this->sortFlags);
    }

    protected function dataToObjects($objectStoreClass)
    {
        foreach ($this->data AS $dataKey => $dataValue) {
            $object = new $objectStoreClass();
            foreach ($dataValue AS $dataValueKey => $data) {
                $object->$dataValueKey = $data;
            }
            $this->data[$dataKey] = $object;
        }
    }

}